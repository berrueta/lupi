header {
  // code at the beginning of any generated file
  package org.asturlinux.berrueta.lupi;

  import java.io.*;
  import java.util.HashMap;
  import java.util.LinkedList;
  import java.util.Collection;
}

{
}

//
// ======================================================================
// Lexer
// ======================================================================
//


class LupiLexer extends Lexer;

options {
  charVocabulary = '\0'..'\377';
  testLiterals=false;
  k=4;
}

protected ALPHA_UPPER : 'A'..'Z' ;
protected ALPHA_LOWER : 'a'..'z' ;
protected ALPHANUMERIC : 'A'..'Z'|'a'..'z'|'0'..'9';


// Whitespace
WS
  : ( ' '
    | '\t'
    | '\f'
    | '\n' { newline(); }
    | '\15'
    ) { $setType(Token.SKIP); }
    ;

// Symbols
DOT       : '.' ;
ARROW     : ":-";
COMMA     : ',';
OPEN_PAR  : '(';
CLOSE_PAR : ')';
OPEN_COR  : '[';
CLOSE_COR : ']';
BAR       : '|';

// IDENTIFIERS
IDENT
  options { testLiterals=true; paraphrase = "id"; }
  : ALPHA_LOWER (ALPHANUMERIC)*
  ;

VAR
  options { testLiterals=true; paraphrase = "var"; }
  : ALPHA_UPPER (ALPHANUMERIC)*
  ;



// =======================================================================
// Parser
// =======================================================================

class LupiParse extends Parser;
options {
  defaultErrorHandler = false;
  k=2;
}

{
  // atributos del sintactico
  private LupiLexer lexer_;

  public void setLexer(LupiLexer lexer)
  {
    lexer_ = lexer;
  }

  public LupiLexer getLexer()
  {
    return lexer_;
  }

}


// ===================================================================
// REGLAS PRINCIPALES
// ===================================================================


program_
returns [ Program program ]
options { defaultErrorHandler = false; }
{
  program = new Program();
  Rule rule = null;
}
  : (rule = rule_ { program.addRule(rule); } )*
  ;

query_
returns [ LinkedList predicate_list ]
{
  HashMap variables = new HashMap();
}
  : predicate_list = predicate_list_ [ variables ]
  ;

rule_
returns [ Rule rule ]
{
  Predicate head = null;
  LinkedList tail = new LinkedList();
  HashMap variables = new HashMap();
}
  : head = predicate_ [ variables ]
    ( ARROW tail = predicate_list_ [ variables ] )? DOT
  { rule = new Rule(head,tail); }
  ;

predicate_list_
[ HashMap variables ]
returns [ LinkedList predicate_list ]
{
  Predicate predicate = null;
  predicate_list = new LinkedList();
}
  : predicate = predicate_ [ variables ] { predicate_list.add(predicate); }
    ( COMMA predicate = predicate_ [ variables ] { predicate_list.add(predicate); } )*
  ;

predicate_
[ HashMap variables ]
returns [ Predicate predicate ]
{ 
  Collection atom_list = null;
}
  : id : IDENT OPEN_PAR atom_list = atom_list_ [ variables ] CLOSE_PAR
    { predicate = new Predicate(id.getText(),atom_list); }
  ;

atom_list_
[ HashMap variables ]
returns [ Collection atoms ]
{ 
  atoms = new LinkedList();
  Atom atom = null;
}
  : ( atom = atom_ [ variables ] { atoms.add(atom); }
      ( COMMA atom = atom_ [ variables ] { atoms.add(atom); } )*
    )?
  ;

atom_
[ HashMap variables ]
returns [ Atom atom ]
  : atom = variable_ [ variables ]
  | atom = function_ [ variables ]
  | atom = constant_
  | atom = list_     [ variables ]
  ;

variable_
[ HashMap variables ]
returns [ Atom atom ]
  : id : VAR
    { if ( !variables.containsKey(id.getText()) ) {
        variables.put(id.getText(),new Variable(id.getText()));
      }
      atom = (Variable)variables.get(id.getText());
    }
  ;

constant_
returns [ Atom atom ]
  : id : IDENT
    { atom = new Constant(id.getText()); }
  ;

function_
[ HashMap variables ]
returns [ Atom atom ]
{
  Collection atom_list = null;
}
  : id : IDENT 
    OPEN_PAR atom_list = atom_list_ [ variables ] CLOSE_PAR
    { atom = new Function(id.getText(),atom_list); }
  ;


// No se necesita modificar el int�rprete para soportar listas, simplemente
// le a�adimos "sintactic sugar" para simplificar la sintaxis de las listas

list_
[ HashMap variables ]
returns [ Atom atom ]
{
}
  : OPEN_COR atom = inside_list_ [ variables ] CLOSE_COR
  | OPEN_COR CLOSE_COR  { atom = new Constant(ListFunction.EMPTY_LIST_CONSTANT_ID); }
  ;

inside_list_
[ HashMap variables ]
returns [ Atom atom ]
{
  Atom car = null;
  Atom cdr = new Constant(ListFunction.EMPTY_LIST_CONSTANT_ID);
}
  : car = atom_ [ variables ]            
    ( BAR   cdr = atom_        [ variables ]
    | COMMA cdr = inside_list_ [ variables ]
    )?
  { atom = new ListFunction(car,cdr); }
  ;