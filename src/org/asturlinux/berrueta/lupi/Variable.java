/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 */

package org.asturlinux.berrueta.lupi;

import java.util.LinkedList;
import java.util.Collection;
import org.asturlinux.frade.prolix.interpreter.interfaces.Substitution;
import java.util.Set;
import java.util.HashSet;



/**
 * Variable.java
 *
 *
 * Created: Fri Aug 15 19:17:23 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class Variable extends Atom {

  public Variable(String id) {
    super(id);
  } // Variable constructor

  public Set extractVariables() {
    HashSet variables = new HashSet();
    variables.add(this);
    return variables;
  }

  public boolean isVariable() {
    return true;
  }

  public Atom duplicate() {
    return new Variable(getId());
  }
  
  public Collection unify(Atom atom, Collection substitutions) {
    if ( atom.extractVariables().contains(this) )
      return null;

    LupiSubstitution substitution = LupiSubstitution.find(this,substitutions);
    if (substitution!=null) {
      return substitution.getNewAtom().unify(atom,substitutions);
    } // end of if (substitution!=null)
    else {
      return LupiSubstitution.compose(substitutions,new LupiSubstitution(this,atom.applySubstitutions(substitutions)));      
    } // end of else
  }

} // Variable
