/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import org.asturlinux.frade.prolix.interpreter.interfaces.PrologInterpreter;
import org.asturlinux.frade.prolix.interpreter.exceptions.CreateException;
import org.asturlinux.frade.prolix.interpreter.interfaces.PrologContext;
import org.asturlinux.frade.prolix.interpreter.interfaces.TreeElement;



/**
 * LupiPrologInterpreter.java
 *
 *
 * Created: Fri Aug 15 18:31:21 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class LupiPrologInterpreter implements PrologInterpreter {
  public LupiPrologInterpreter() {
    
  } // LupiPrologInterpreter constructor
  

  public PrologContext createContext()
    throws CreateException {
    return new LupiPrologContext();
  }

  public static void main(String[] args) {

    try {
      PrologInterpreter interpreter = new LupiPrologInterpreter();
      PrologContext context = interpreter.createContext();

      String program = "";
      program += "padre(fred,alice).\n";
      program += "padre(fred,bob).\n";
      program += "hermano(X,Y) :- padre(Z,X),padre(Z,Y).\n";
      program += "sum(zero,X,X).\n";
      program += "sum(sig(X),Y,sig(Z)):-sum(X,Y,Z).\n";

      program += "concat([],Z,Z).\n";
      program += "concat([X|Y],Z,[X|W]):-concat(Y,Z,W).\n";

      program += "length([],zero).\n";
      program += "length([X|Y],sig(Z)):-length(Y,Z).\n";

      program += "reverse([],[]).\n";
      program += "reverse([X|Xs],Y):-reverse(Xs,Z),concat(Z,[X],Y).\n";

      context.load(program);

//      String query = "sum(sig(sig(zero)),sig(sig(sig(zero))),X)";
      String query = "concat([a,b,c],[d,e],R),length(R,L),reverse(R,R2)";
      context.consult(query);

      TreeElement result = null;
      int iteration = 1;
      do {
	System.out.println("\n\nPERFORMING STEP (iteation=" + iteration + "):\n");
	result = context.step();
	System.out.println("\n\nRESULT (iteration=" + iteration + "):\n");
	System.out.println(result.toString());
	iteration++;
      } while (!result.isCompletlyExplored());
	
    } catch (Exception e) {
      e.printStackTrace();
    } // end of try-catch
      
  } // end of main()

} // LupiPrologInterpreter
