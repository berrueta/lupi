/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import org.asturlinux.frade.prolix.interpreter.interfaces.PrologContext;
import org.asturlinux.frade.prolix.interpreter.exceptions.SyntaxException;
import org.asturlinux.frade.prolix.interpreter.exceptions.LexicalException;
import org.asturlinux.frade.prolix.interpreter.exceptions.ProgramAlreadyLoadedException;
import org.asturlinux.frade.prolix.interpreter.exceptions.ProgramNotLoadedException;
import org.asturlinux.frade.prolix.interpreter.exceptions.QueryNotLoadedException;
import org.asturlinux.frade.prolix.interpreter.interfaces.TreeElement;
import antlr.TokenStreamRecognitionException;
import antlr.RecognitionException;
import antlr.MismatchedTokenException;
import antlr.NoViableAltException;
import antlr.TokenStreamException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collection;
import java.util.LinkedList;

/**
 * LupiPrologContext.java
 *
 *
 * Created: Fri Aug 15 18:12:37 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class LupiPrologContext implements PrologContext {

  private Program _program = null;
  private Collection _query = null;
  private LupiTreeElement _root = null;

  public LupiPrologContext() {

  } // LupiPrologContext constructor

  public void load (String programSource)
    throws SyntaxException, LexicalException, ProgramAlreadyLoadedException {

    System.out.println("DEBUG: loading program:\n" + programSource);

    Reader input = new StringReader(programSource);

    LupiLexer lexer = new LupiLexer(input);

    LupiParse parser = new LupiParse(lexer); 
    parser.setLexer(lexer);

    try {
      _program = parser.program_(); // the beauty of simmetric code...
    }
    catch(TokenStreamRecognitionException tsre) {
      RecognitionException recog = tsre.recog;
      System.out.println("DEBUG: " + recog);
      throw new LexicalException(recog.toString());
    }
    catch(MismatchedTokenException mte) {
      System.out.println("DEBUG: " + mte);
      throw new SyntaxException(mte.toString());
    }
    catch(NoViableAltException nvae) {
      System.out.println("DEBUG: " + nvae);
      throw new SyntaxException(nvae.toString());
    }
    catch (TokenStreamException tse) {
      System.err.println("TOKEN STREAM EXCEPTION: " + tse);       
    } // end of catch
    
    catch (RecognitionException re) {
      System.err.println("RECOGNITION EXCEPTION: " + re);
    } // end of catch

    /*    catch(Exception e) {
	  System.err.println("UNKNOWN ERROR: " + e);
	  System.err.println(e.getClass());
	  e.printStackTrace();
	  }*/

    System.out.println("DEBUG PROGRAM:\n" + _program);

  }

  public void consult (String query)
    throws SyntaxException, LexicalException, ProgramNotLoadedException {

    System.out.println("DEBUG: loading query:" + query);

    if (_program==null) {
      throw new ProgramNotLoadedException();
    } // end of if (_program==null)
    

    Reader input = new StringReader(query);

    LupiLexer lexer = new LupiLexer(input);

    LupiParse parser = new LupiParse(lexer); 
    parser.setLexer(lexer);

    try {
      _query = parser.query_();
    }
    catch(TokenStreamRecognitionException tsre) {
      RecognitionException recog = tsre.recog;
      throw new LexicalException(recog.toString());
    }
    catch(MismatchedTokenException mte) {
      throw new SyntaxException(mte.toString());
    }
    catch(NoViableAltException nvae) {
      throw new SyntaxException(nvae.toString());
    }
    catch (TokenStreamException tse) {
      System.err.println("TOKEN STREAM EXCEPTION: " + tse);       
    } // end of catch
    
    catch (RecognitionException re) {
      System.err.println("RECOGNITION EXCEPTION: " + re);
    } // end of catch

    /*    catch(Exception e) {
	  System.err.println("UNKNOWN ERROR: " + e);
	  System.err.println(e.getClass());
	  e.printStackTrace();
	  }*/

    System.out.println("DEBUG QUERY:\n" + Predicate.toString(_query));

    _root = new LupiTreeElement(null,_query,new LinkedList(),
				_program.getRules().iterator());

  }
     
  public TreeElement step() {
    _root.step(_program);
    return _root;
  }
  
} // LupiPrologContext
