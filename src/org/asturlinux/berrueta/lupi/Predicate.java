/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;



/**
 * Predicate.java
 *
 *
 * Created: Fri Aug 15 18:58:23 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class Predicate {

  private String _id;
  private LinkedList _subexpressions;

  public Predicate(String id, Collection subexpressions) {
    _id = id;
    _subexpressions = new LinkedList(subexpressions);
  } // Predicate constructor

  public String getId() {
    return _id;
  }
  
  public Collection getSubexpressions() {
    return _subexpressions;
  }

  public int getArity() {
    return _subexpressions.size();
  }

  public String toString() {
    StringBuffer subexpressionsString = new StringBuffer();
    Iterator iterator = _subexpressions.iterator();
    boolean first = true;
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      if (!first) {
	subexpressionsString.append(",");
      } // end of if (!first)
      else {
	first = false;
      } // end of else
      
      subexpressionsString.append(subexpression.toString());
    } // end of while (iterator.hasNext())
    
    return _id + "(" + subexpressionsString + ")";
  }

  public static String toString(Collection predicates) {
    StringBuffer predicatesString = new StringBuffer();
    Iterator iterator = predicates.iterator();
    boolean first = true;
    while (iterator.hasNext()) {
      Predicate predicate = (Predicate)iterator.next();
      if (!first) {
	predicatesString.append(",");
      } // end of if (!first)
      else {
	first = false;	
      } // end of else

      predicatesString.append(predicate.toString());
    } // end of while (iterator.hasNext())
    
    return predicatesString.toString();
  }

  public Collection unify(Predicate predicate) {

    System.out.println("DEBUG: trying to unify " + this + " and " + predicate);

    if (!getId().equals(predicate.getId())) {
      return null; // cannot unify
    } // end of if (!getId().equals(predicate.getId()))

    if (getArity() != predicate.getArity()) {
      return null;
    } // end of if (getArity() != predicate.getArity())

    Collection substitutions = new LinkedList();

    Iterator iterator1 = getSubexpressions().iterator();
    Iterator iterator2 = predicate.getSubexpressions().iterator();
    while (iterator1.hasNext() && iterator2.hasNext() && substitutions!=null) {
      Atom atom1 = (Atom)iterator1.next();
      Atom atom2 = (Atom)iterator2.next();
      substitutions = atom1.unify(atom2,substitutions);
    } // end of while (iterator1.hasNext() && iterator2.hasNext())

    System.out.println("DEBUG: trying to unify " + this + " and " + predicate + " results in " + substitutions);
    
    return substitutions;
  }

  public static Collection applySubstitutions(Collection predicates,
					      Collection substitutions) {
    LinkedList newList = new LinkedList();
    Iterator iterator = predicates.iterator();
    while (iterator.hasNext()) {
      Predicate predicate = (Predicate)iterator.next();
      newList.add(predicate.applySubstitutions(substitutions));
    } // end of while (iterator.hasNext())
    
    return newList;
  }

  public Predicate applySubstitutions(Collection substitutions) {
    LinkedList newSubexpressions = new LinkedList();
    Iterator iterator = _subexpressions.iterator();
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      newSubexpressions.add(subexpression.applySubstitutions(substitutions));
    } // end of while (iterator.hasNext())
    
    Predicate newPredicate = new Predicate(getId(),newSubexpressions);
    return newPredicate;
  }

  public static Set extractVariables(Collection predicates) {
    HashSet variables = new HashSet();
    Iterator iterator = predicates.iterator();
    while (iterator.hasNext()) {
      Predicate predicate = (Predicate)iterator.next();
      variables.addAll(predicate.extractVariables());
    } // end of while (iterator.hasNext())
    
    return variables;
  }

  public Set extractVariables() {
    HashSet variables = new HashSet();
    Iterator iterator = _subexpressions.iterator();
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      variables.addAll(subexpression.extractVariables());
    } // end of while (iterator.hasNext())
    
    return variables;
  }
  
} // Predicate
