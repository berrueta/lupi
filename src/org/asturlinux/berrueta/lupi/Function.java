/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 */

package org.asturlinux.berrueta.lupi;

import java.util.LinkedList;
import java.util.Collection;
import java.util.Iterator;
import org.asturlinux.frade.prolix.interpreter.interfaces.Substitution;
import java.util.Set;
import java.util.HashSet;



/**
 * Function.java
 *
 *
 * Created: Fri Aug 15 19:11:59 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class Function extends Atom {

  private LinkedList _subexpressions;

  public Function(String id, Collection subexpressions) {
    super(id);
    _subexpressions = new LinkedList(subexpressions);
  } // Function constructor

  public int getArity() {
    return _subexpressions.size();
  }

  public Collection getSubexpressions() {
    return _subexpressions;
  }


  public boolean isVariable() {
    return false;
  }

  public String toString() {
    StringBuffer subexpressionsString = new StringBuffer();
    Iterator iterator = _subexpressions.iterator();
    boolean first = true;
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      if (!first) {
	subexpressionsString.append(",");
      } // end of if (!first)
      else {
	first = false;	
      } // end of else
      
      subexpressionsString.append(subexpression.toString());
    } // end of while (iterator.hasNext())
    
    if (_subexpressions.size()==0) {
      return getId();
    } // end of if (_subexpressions.size()==0)
    else {
      return getId() + "(" + subexpressionsString + ")";
    } // end of else
  }

  public Set extractVariables() {
    HashSet variables = new HashSet();
    Iterator iterator = _subexpressions.iterator();
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      variables.addAll(subexpression.extractVariables());
    } // end of while (iterator.hasNext())
    return variables;
  }
  
  public Atom applySubstitution(LupiSubstitution substitution) {
    LinkedList newSubexpressions = new LinkedList();
    Iterator iterator = _subexpressions.iterator();
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      newSubexpressions.add(subexpression.applySubstitution(substitution));
    } // end of while (iterator.hasNext())
    return new Function(getId(),newSubexpressions);
  }
  
  public Atom duplicate() {
    LinkedList newSubexpressions = new LinkedList();
    Iterator iterator = _subexpressions.iterator();
    while (iterator.hasNext()) {
      Atom subexpression = (Atom)iterator.next();
      newSubexpressions.add(subexpression.duplicate());
    } // end of while (iterator.hasNext())
    return new Function(getId(),newSubexpressions);
  }

  public Collection unify(Atom atom, Collection substitutions) {
    if (atom.isVariable()) {
      return atom.unify(this,substitutions);
    } // end of if (atom.isVariable())
    else {

      Function otherFunction = (Function)atom;

      if (!getId().equals(otherFunction.getId())) {
	return null;
      } // end of if (!getId().equals(otherFunction.getId()))

      if (getArity()!=otherFunction.getArity()) {
	return null;
      } // end of if (getArity()!=otherFunction.getArity())

      Collection result = substitutions;

      Iterator iterator1 = getSubexpressions().iterator();
      Iterator iterator2 = otherFunction.getSubexpressions().iterator();
      while (iterator1.hasNext() && iterator2.hasNext() && substitutions!=null) {
	Atom atom1 = (Atom)iterator1.next();
	Atom atom2 = (Atom)iterator2.next();
	substitutions = atom1.unify(atom2,substitutions);
      } // end of while (iterator1.hasNext() && iterator2.hasNext())

      return substitutions;

    } // end of else
    
  }
  
} // Function
