/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import org.asturlinux.frade.prolix.interpreter.interfaces.TreeElementImpl;
import org.asturlinux.frade.prolix.interpreter.interfaces.Substitution;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Collection;
import org.asturlinux.frade.prolix.interpreter.interfaces.TreeElement;
import java.util.Set;

/**
 * LupiTreeElement.java
 *
 *
 * Created: Fri Aug 15 18:41:05 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class LupiTreeElement extends TreeElementImpl {

  private LupiTreeElement _parent;
  private Iterator _ruleIterator;
  private LinkedList _children = new LinkedList();
  private LinkedList _query;
  private LinkedList _substitutions;
  private int _level;

  public LupiTreeElement(LupiTreeElement parent,
			 Collection query,
			 Collection substitutions,
			 Iterator ruleIterator) {
    _parent = parent;
    _query = new LinkedList(query);
    _substitutions = new LinkedList(substitutions);
    _ruleIterator = ruleIterator;
    if (_parent==null) {
      _level = 0;
    } // end of if (_parent==null)
    else {
      _level = _parent.getLevel() + 1;
    } // end of else
    
    
  } // LupiTreeElement constructor

  public boolean isCompletlyExplored() {
    return isSolution() ||
      (!_ruleIterator.hasNext() &&
       ( _children.size() == 0 ||
	 ((LupiTreeElement)_children.getLast()).isCompletlyExplored()
	 )
       );
  }

  public boolean isFinal() {
    return isCompletlyExplored() && _children.size()==0;
  }

  public boolean isSolution() {
    return _query.size()==0;
  }
     
  public TreeElement[] getNextLevelElements() {
    return (TreeElement[])_children.toArray(new TreeElement[_children.size()]);
  }

  public TreeElement getParent() {
    return _parent;
  }
 
  public String getQuery() {
    return Predicate.toString(_query);
  }

  public Collection getQueryPredicates() {
    return _query;
  }

  public Substitution[] getSubstitutions() {
    //    return (Substitution[])_substitutions.toArray(new Substitution[_substitutions.size()]);

    // locates tree root
    TreeElement root = this;
    while (root.getParent()!=null) {
      root = root.getParent();
    } // end of while (root.getParent()!=null)
    
    // extracts query variables
    Set variables = Predicate.extractVariables(((LupiTreeElement)root).getQueryPredicates());

    // returns the subset of substitutions
    Collection subset = LupiSubstitution.subset(_substitutions,variables);
    return (Substitution[])subset.toArray(new Substitution[subset.size()]);
  }

  public int getLevel() {
    return _level;
  }

  public boolean step(Program program) {

    System.out.println("DEBUG: step in query=" + _query + " at level " + _level);

    // give a chance to last generated child
    if ( _children.size() > 0 ) {
      LupiTreeElement child = (LupiTreeElement)_children.getLast();
      if ( child.step(program) ) {
	return true; // step on the branch
      }
    }

    // tries to generate another children
    if (_query.size() > 0) {      
      Predicate currentPredicate = (Predicate)_query.getFirst();

      while (_ruleIterator.hasNext()) {
	Rule rule = (Rule)_ruleIterator.next();

	// extracts rule variables
	Set ruleVariables = rule.extractVariables();
	Collection renameSubstitution =
	  LupiSubstitution.createRenameSubstitution(ruleVariables);

	// rewrites rule
	Rule rewrittenRule = rule.applySubstitutions(renameSubstitution);

	// tries to unify
	//	Collection substitutions = currentPredicate.unify(rewrittenRule.getHead());
	Collection substitutions = rewrittenRule.getHead().unify(currentPredicate);

	if (substitutions!=null) {
	
	  LinkedList newQuery = new LinkedList();
	  newQuery.addAll(rewrittenRule.getTail());
	  Iterator queryIterator = _query.iterator();
	  queryIterator.next(); // discards first element
	  while (queryIterator.hasNext()) {
	    newQuery.add(queryIterator.next());
	  } // end of while (queryIterator.hasNext())

	  Collection newSubstitutions = LupiSubstitution.compose(_substitutions,
								 substitutions);

	  Collection newQuery2 =
	    Predicate.applySubstitutions(newQuery,newSubstitutions);

	  LupiTreeElement newChild =
	    new LupiTreeElement(this,newQuery2,newSubstitutions,
				program.getRules().iterator());
	  _children.add(newChild);
	  System.out.println("DEBUG: created new node: " + newChild);
	  // FIXME!!

	  return true;
	   
	} // end of if (substitutions!=null)
	
      } // end of while (_iterator.hasNext())
	
    } // end of if (_query.size() > 0)
      
    return false; // unable to step

  }

  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("TREE ELEMENT LEVEL " + getLevel() + "\n");
    stringBuffer.append("  Query: " + getQuery() + "\n");
    stringBuffer.append("  Substitutions: ");
    Substitution[] substitutions = getSubstitutions();
    for (int i = 0 ; i<substitutions.length ; i++) {
      stringBuffer.append(" " + substitutions[i]);
    } // end of for (int i ; i<substitutions.length ; i++)
    stringBuffer.append("\n");
    stringBuffer.append("  Is solution: " + isSolution() + "\n");
    stringBuffer.append("  Is completly explored: " + isCompletlyExplored() + "\n");
    stringBuffer.append("  Is final: " + isFinal() + "\n\n");
    Iterator iterator = _children.iterator();
    while (iterator.hasNext()) {
      LupiTreeElement child = (LupiTreeElement)iterator.next();
      stringBuffer.append(child.toString());
    } // end of while (iterator.hasNext())

    return stringBuffer.toString();
    
  }

} // LupiTreeElement
