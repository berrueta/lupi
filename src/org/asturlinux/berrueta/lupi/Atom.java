/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import java.util.Collection;
import org.asturlinux.frade.prolix.interpreter.interfaces.Substitution;
import java.util.Iterator;
import java.util.Set;



/**
 * Atom.java
 *
 *
 * Created: Fri Aug 15 19:06:41 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public abstract class Atom {

  private String _id;

  public Atom(String id) {
    _id = id;
  } // Atom constructor
  
  public String getId() {
    return _id;
  }

  public void setId(String id) {
    _id = id;
  }

  public String toString() {
    return getId();
  }

  public abstract Collection unify(Atom atom, Collection substitutions);
  public abstract Set extractVariables();
  public abstract Atom duplicate();
  public abstract boolean isVariable();

  public Atom applySubstitution(LupiSubstitution substitution) {
    if (substitution.getOriginalAtom().equals(this)) {
      return substitution.getNewAtom().duplicate();
    } // end of if (substitution.getOriginalAtom().equals(getId()))
    else {
      return this.duplicate();
    } // end of else
    
  }

  public Atom applySubstitutions(Collection substitutions) {
    Atom newAtom = this;
    Iterator iterator = substitutions.iterator();
    while (iterator.hasNext()) {
      LupiSubstitution substitution = (LupiSubstitution)iterator.next();
      newAtom = newAtom.applySubstitution(substitution);
    } // end of while (iterator.hasNext())
    return newAtom;
  }

  public boolean equals(Object o) {
    return toString().equals(o.toString());
  }

} // Atom
