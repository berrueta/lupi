/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

/**
 * LupiSubstitution.java
 *
 *
 * Created: Fri Aug 15 18:36:33 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */

import org.asturlinux.frade.prolix.interpreter.interfaces.Substitution;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Set;

public class LupiSubstitution implements Substitution {

  private Atom _originalAtom;
  private Atom _newAtom;

  public LupiSubstitution(Atom originalAtom, Atom newAtom) {
    _originalAtom = originalAtom;
    _newAtom = newAtom;
  } // LupiSubstitution constructor

  public Atom getOriginalAtom() {
    return _originalAtom;
  }

  public String getOriginalValue() {
    return _originalAtom.toString();
  }

  public Atom getNewAtom() {
    return _newAtom;
  }

  public String getNewValue() {
    return _newAtom.toString();
  }

  public static Collection createRenameSubstitution(Collection variables) {
    LinkedList substitutions = new LinkedList();
    Iterator iterator = variables.iterator();
    while (iterator.hasNext()) {
      Variable variable = (Variable)iterator.next();
      Variable newVariable = VariableFactory.createVariable();
      substitutions.add(new LupiSubstitution(variable,newVariable));
    } // end of while (iterator.hasNext())
    
    return substitutions;
  }

  public static Collection compose(Collection substitutions,
				   LupiSubstitution newSubstitution) {
    LinkedList newSubstitutions = new LinkedList();
    Iterator iterator = substitutions.iterator();
    while (iterator.hasNext()) {
      LupiSubstitution substitution = (LupiSubstitution)iterator.next();
      newSubstitutions.add(new LupiSubstitution
			   (substitution.getOriginalAtom(),
			    substitution.getNewAtom().applySubstitution
			    (newSubstitution)
			    )
			   );
    } // end of while (iterator.hasNext())
    
    // adds the new substitution
    newSubstitutions.add(newSubstitution);
    return newSubstitutions;
  }

  public static Collection compose(Collection substitutions1,
				   Collection substitutions2) {
    Collection result = substitutions1;
    Iterator iterator = substitutions2.iterator();
    while (iterator.hasNext()) {
      LupiSubstitution substitution = (LupiSubstitution)iterator.next();
      result = LupiSubstitution.compose(result,substitution);
    } // end of while (iterator.hasNext())
    return result;
  }

  public static LupiSubstitution find(Atom atom, Collection substitutions) {
    LupiSubstitution result = null;
    Iterator iterator = substitutions.iterator();
    while (iterator.hasNext() && result==null) {
      LupiSubstitution substitution = (LupiSubstitution)iterator.next();
      if (atom.equals(substitution.getOriginalAtom())) {
	result = substitution;
      } // end of if (atom.equals(substitution.getOriginalAtom()))      
    } // end of while (iterator.hasNext())
    return result;
  }

  public static Collection subset(Collection substitutions,
				  Set variables) {
    LinkedList subset = new LinkedList();
    Iterator iterator = variables.iterator();
    while (iterator.hasNext()) {
      Variable variable = (Variable)iterator.next();
      Substitution substitution = LupiSubstitution.find(variable,substitutions);
      if (substitution!=null) {
	subset.add(substitution);
      } // end of if (substitution!=null)
    } // end of while (iterator.hasNext())

    return subset;    
  }

  /*
    public static Collection reverse(Collection substitutions) {
    LinkedList newList = new LinkedList();
    Iterator iterator = substitutions.iterator();
    while (iterator.hasNext()) {
    LupiSubstitution substitution = (LupiSubstitution)iterator.next();
    newList.add(substitution.reverse());
    } // end of while (iterator.hasNext())
    
    return newList;
    }

    public Substitution reverse() {
    return new LupiSubstitution(this.getNewAtom(),this.getOriginalAtom());
    }
  */

  public String toString() {
    return "{" + _originalAtom + "/" + _newAtom + "}";
  }
  
} // LupiSubstitution
