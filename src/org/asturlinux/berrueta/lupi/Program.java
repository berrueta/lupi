/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import java.util.LinkedList;
import java.util.Collection;
import java.util.Iterator;



/**
 * Program.java
 *
 *
 * Created: Fri Aug 15 19:39:29 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class Program {

  private LinkedList _rules = new LinkedList();

  public Program() {
    
  } // Program constructor

  public Collection getRules() {
    return _rules;
  }

  public void addRule(Rule rule) {
    _rules.add(rule);
  }

  public String toString() {
    StringBuffer programString = new StringBuffer();
    Iterator iterator = _rules.iterator();
    while (iterator.hasNext()) {
      Rule rule = (Rule)iterator.next();
      programString.append(rule.toString() + "\n");
    } // end of while (iterator.hasNext())
    
    return programString.toString();    
  }
  
} // Program
