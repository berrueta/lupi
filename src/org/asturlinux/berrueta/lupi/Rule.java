/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

import java.util.LinkedList;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;



/**
 * Rule.java
 *
 *
 * Created: Fri Aug 15 19:34:10 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class Rule {

  private Predicate _head;
  private LinkedList _tail;

  public Rule(Predicate head, Collection tail) {
    _head = head;
    _tail = new LinkedList(tail);
  } // Rule constructor

  public Predicate getHead() {
    return _head;
  }

  public Collection getTail() {
    return _tail;
  }

  public String toString() {
    if (_tail.size()==0) {
      return _head.toString() + ".";       
    } // end of if (_tail.size==0)
    else {
      return _head.toString() + ":-" + Predicate.toString(_tail) + ".";
    } // end of else
    
  }

  public Set extractVariables() {
    HashSet variables = new HashSet();
    variables.addAll(getHead().extractVariables());
    variables.addAll(Predicate.extractVariables(getTail()));
    return variables;
  }

  public Rule applySubstitutions(Collection substitutions) {
    Predicate newHead = getHead().applySubstitutions(substitutions);
    Collection newTail = Predicate.applySubstitutions(getTail(),substitutions);
    Rule newRule = new Rule(newHead,newTail);
    return newRule;
  }

} // Rule
