/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 */

package org.asturlinux.berrueta.lupi;

import java.util.LinkedList;
import java.util.Collection;
import java.util.Iterator;
import org.asturlinux.frade.prolix.interpreter.interfaces.Substitution;
import java.util.Set;
import java.util.HashSet;



/**
 * ListFunction.java
 *
 *
 * Created: Fri Sep 09
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class ListFunction extends Function {

  public static final String LIST_FUNCTION_ID = "list";
  public static final String EMPTY_LIST_CONSTANT_ID = "[]";


  public ListFunction(Atom car, Atom cdr) {
    super(LIST_FUNCTION_ID,new LinkedList());
    getSubexpressions().add(car);
    getSubexpressions().add(cdr);
  } // ListFunction constructor

  public Atom getCar() {
    Iterator i = getSubexpressions().iterator();
    return (Atom)i.next();
  }

  public Atom getCdr() {
    Iterator i = getSubexpressions().iterator();
    i.next(); // discards first
    return (Atom)i.next();
  }

  public String toString() {

    StringBuffer insideListString = new StringBuffer();

    ListFunction current = this;
    do {
      insideListString.append(current.getCar().toString());

      Atom cdr = current.getCdr();
      if (cdr.getId().equals(LIST_FUNCTION_ID)) {
	insideListString.append(",");
	current = (ListFunction)current.getCdr();
      } // end of if (cdr.getId() == LIST_FUNCTION_ID)
      else {
	if (!cdr.getId().equals(EMPTY_LIST_CONSTANT_ID)) {	
	  insideListString.append("|");
	  insideListString.append(current.getCdr().toString());
	} // end of if (cdr.getId()==EMPTY_LIST_CONSTANT_ID)
	current = null;
      } // end of else
    }
    while (current!=null);

    return "[" + insideListString + "]";
  }

  public Atom duplicate() {
    return new ListFunction(getCar().duplicate(),getCdr().duplicate());
  }

  public Atom applySubstitution(LupiSubstitution substitution) {
    return new ListFunction(getCar().applySubstitution(substitution),
			    getCdr().applySubstitution(substitution));
  }
  
  
} // Function
