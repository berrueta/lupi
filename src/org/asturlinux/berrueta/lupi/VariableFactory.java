/**
 *
 * Copyright 2003 Diego Berrueta
 * 
 * This file is part of LUpi.
 *
 * LUpi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prolix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prolix; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 **/

package org.asturlinux.berrueta.lupi;

/**
 * VariableFactory.java
 *
 *
 * Created: Fri Aug 15 19:48:58 2003
 *
 * @author <a href="mailto:berrueta@asturlinux.org">Diego Berrueta</a>
 * @version 1.0
 */
public class VariableFactory {

  private static int _num = 0;

  public VariableFactory() {
    
  } // VariableFactory constructor

  public static Variable createVariable(String id) {
    return new Variable(id);
  }

  public static Variable createVariable() {
    String generatedId = "_" + (_num++);
    return createVariable(generatedId);
  }
  
} // VariableFactory
